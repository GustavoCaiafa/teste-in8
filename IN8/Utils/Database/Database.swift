//
//  Database.swift
//  IN8
//
//  Created by eWorld Tecnologia on 12/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import Foundation
import SQLite

class Database{
    
    static let instance = Database()
    private let db: Connection?
    
    private let TabelaPessoa = Table("Pessoa")
    private let PessoaId = Expression<Int64>("PessoaId")
    private let Nome = Expression<String?>("Nome")
    private let Email = Expression<String?>("Email")
    private let Senha = Expression<String?>("Senha")
    
    private let TabelaConversoes = Table("Conversao")
    private let ConversaoId = Expression<Int64>("ConversaoId")
    private let MoedaBase = Expression<String?>("MoedaBase")
    private let ValorBase = Expression<Double?>("ValorBase")
    private let MoedaTarget = Expression<String?>("MoedaTarget")
    private let NovoValorGerado = Expression<Double?>("NovoValorGerado")
    private let EstrangeiraPessoaId = Expression<Int64?>("EstrangeiraPessoaId")
    
    
    private init() {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
        ).first!
        do {
            db = try Connection("\(path)/Database.sqlite3")
        } catch {
            db = nil
            print ("não conseguiu abrir o database")
        }
    }
    
    func deletarBanco() -> Bool{
        do {
            let tabelas = [TabelaPessoa,TabelaConversoes]
            for tabela in tabelas {
                try db?.run(tabela.drop())
                print("Deletou a tabela\(tabela)")
            }
            return true
        } catch {
            print("delete failed: \(error)")
            return false
        }
    }
    
    func criarTabelaPessoa() -> Bool{
        do {
            try db!.run(TabelaPessoa.create(ifNotExists: true) { table in
                table.column(PessoaId, primaryKey: true)
                table.column(Nome)
                table.column(Email)
                table.column(Senha)
            })
            return true
        } catch {
            print("Nao conseguiu criar a tabela pessoa: \(error)")
            return false
        }
    }
    
    func criarTabelaConversao() -> Bool{
        do {
            try db!.run(TabelaConversoes.create(ifNotExists: true) { table in
                table.column(ConversaoId, primaryKey: true)
                table.column(MoedaBase)
                table.column(ValorBase)
                table.column(MoedaTarget)
                table.column(NovoValorGerado)
                table.column(EstrangeiraPessoaId)
                table.foreignKey(EstrangeiraPessoaId, references: TabelaPessoa, PessoaId)
            })
            return true
        } catch {
            print("Nao conseguiu criar a tabela conversao: \(error)")
            return false
        }
    }
    
    func addPessoa(nome: String?,email: String?,
                   senha: String?) -> Int64? {
        do {
            
            let resultadoVerificaEmail = verificaEmailExistente(email: email!)
            if(resultadoVerificaEmail != nil){
                if(resultadoVerificaEmail!){
                    return -100
                }
                else{
                    let insert = TabelaPessoa.insert(Nome <- nome,
                                                     Email <- email,
                                                     Senha <- senha)
                    let id = try db!.run(insert)
                    return id
                }
            }
            else{
                return nil
            }
            
        } catch {
            print("Nao conseguiu inserir na tabela pessoa: \(error)")
            return nil
        }
    }
    
    func deletarPessoa(iid: Int64) -> Bool {
        do {
            let itens = TabelaPessoa.filter(PessoaId == iid)
            try db!.run(itens.delete())
            return true
        } catch {
            print("Deletar pessoa falhou \(error)")
            return false
        }
    }
    
    func getPessoa(email : String, senha : String) -> PessoaModel? {
        var pessoaModel = PessoaModel()
        do {
            for pessoa in try db!.prepare(self.TabelaPessoa.filter(Email == email && Senha == senha)) {
                pessoaModel = (PessoaModel(
                    pessoaid: pessoa[PessoaId],
                    nome: pessoa[Nome],
                    email: pessoa[Email],
                    senha: pessoa[Senha]))
            }
            return pessoaModel
        }
        catch {
            print("Get pessoa deu erro")
            return nil
        }
    }
    
    func verificaEmailExistente(email : String) -> Bool?{
        var pessoaid : Int64 = -100
        do{
            for pessoa in try db!.prepare(self.TabelaPessoa.filter(Email == email)) {
                pessoaid = pessoa[PessoaId]
            }
            if(pessoaid == -100){
                return false
            }
            return true
        }
            
        catch{
            print("Erro verificaEmailExistente")
            return nil
        }
    }
    
    func addConversao(moedabase: String?,valorbase: Double?,moedatarget: String?,
                      novovalorgerado: Double?, estrangeirapessoaid: Int64?) -> Bool {
        do {
            let insert = TabelaConversoes.insert(MoedaBase <- moedabase,
                                                 ValorBase <- valorbase,
                                                 MoedaTarget <- moedatarget,
                                                 NovoValorGerado <- novovalorgerado,
                                                 EstrangeiraPessoaId <- estrangeirapessoaid)
            try db!.run(insert)
            return true
        } catch {
            print("Nao conseguiu inserir na tabela conversao: \(error)")
            return false
        }
    }

    func getConversoes(pessoaid : Int64) -> [ConversaoLocalModel]? {
        var conversoes = [ConversaoLocalModel]()
        do {
            for conversao in try db!.prepare(self.TabelaConversoes.filter(EstrangeiraPessoaId == pessoaid)) {
                conversoes.append(ConversaoLocalModel(
                    conversaoid: conversao[ConversaoId],
                    moedabase: conversao[MoedaBase],
                    valorbase: conversao[ValorBase],
                    moedatarget: conversao[MoedaTarget],
                    novovalor: conversao[NovoValorGerado]))
            }
            return conversoes
        }
        catch {
            print("Get pessoa deu erro")
            return nil
        }
    }
}
