//
//  Utils.swift
//  IN8
//
//  Created by eWorld Tecnologia on 09/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

public class Utils{
    
    struct Global {
        static var adicionouConversao = false
    }
}

func showAlertaController(_ controller : UIViewController, texto : String,titulo : String,dismiss:Bool){
    let alertController = UIAlertController(title: titulo, message: texto, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default) { UIAlertAction in
        if(dismiss){
            controller.dismiss(animated: true, completion: nil)
        }
    }
    alertController.addAction(OKAction)
    controller.present(alertController, animated: true) {
    }
}
