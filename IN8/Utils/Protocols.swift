//
//  Protocols.swift
//  IN8
//
//  Created by eWorld Tecnologia on 10/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import Foundation

// Protocolo responsavel por devolver a base selecionada para a home
protocol didSelectBaseProtocol {
    func selectedBase(didSelect: Bool, date: String?, isBase: Bool)
}
