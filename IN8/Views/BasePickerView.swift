//
//  BasePickerView.swift
//  IN8
//
//  Created by eWorld Tecnologia on 10/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

class BasePickerView: UIView {
    
    @IBOutlet weak var dismissView: UIView!{
        didSet{
            let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPickerView))
            self.dismissView.addGestureRecognizer(dismissGesture)
        }
    }
    
    @IBOutlet weak var centerXconstraint: NSLayoutConstraint!
    @IBOutlet var superView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    var pickerViewData = [String]()
    var delegateSelectBaseProtocol : didSelectBaseProtocol?
    var isMoedaBase = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configuracao()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configuracao()
    }
    
    /// Essa funcao esconde a pickerview e retorna o protocolo com o valor selecionado pelo usuario
    @IBAction func BtConfirmar(_ sender: UIButton) {
        let row = pickerView.selectedRow(inComponent: 0)
        let data = pickerViewData[row]
        animaPickerView(constraintValue: 500)
        delegateSelectBaseProtocol?.selectedBase(didSelect: true, date: data, isBase: isMoedaBase)
    }
    
    /// Essa funcao cancela o pick, esconde a pickerview e retorna o protocolo avisando que nao selecionou nada
    @objc private func dismissPickerView(){
        animaPickerView(constraintValue: 500)
        delegateSelectBaseProtocol?.selectedBase(didSelect: false, date: nil, isBase: isMoedaBase)
    }
    
    private func configuracao(){
        Bundle.main.loadNibNamed("BasePickerView", owner: self, options: nil)
        addSubview(superView)
        superView.frame = self.bounds
        superView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    /// Essa funcao seta o delegate e a data da pickerview
    public func configuraPickerView(data : [String], delegate : didSelectBaseProtocol){
        self.pickerViewData = data
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.delegateSelectBaseProtocol = delegate
    }
    
    public func mostrarPickerView(){
        animaPickerView(constraintValue: 0)
    }
    
    private func animaPickerView(constraintValue : CGFloat){
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseIn, animations: {
            self.centerXconstraint.constant = constraintValue
            self.layoutIfNeeded()
        }) { (animationComplete) in
        }
    }
}

extension BasePickerView : UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        pickerViewData[row]
    }
    
}
