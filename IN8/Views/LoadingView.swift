//
//  LoadingView.swift
//  IN8
//
//  Created by eWorld Tecnologia on 10/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: UIView {

    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var animationView: AnimationView!{
        didSet{
            let exchangeAnimation = Animation.named("exchangeAnimation")
            self.animationView.animation = exchangeAnimation
            self.animationView.loopMode = .loop
        }
    }
    
    @IBOutlet weak var animationViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var animationViewHeightConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) { // para xib ou storyboard
        super.init(coder: aDecoder)
        configuracao()
    }
    
    override init(frame: CGRect) { // para inicializacao por codigo
        super.init(frame: frame)
        configuracao()
    }
    
    private func configuracao(){
        Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)
        addSubview(superView)
        superView.frame = self.bounds
        superView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    /// Essa funcao inicia a animacao
    ///
    /// - Warning: Verificacao: apenas se animacao nao é nil ou já está executando
    func play(){
        if(self.animationView.animation != nil && !self.animationView.isAnimationPlaying){
            self.animationView.play()
        }
    }
    
    /// Essa funcao pausa a animacao
    ///
    /// - Warning: Verificacao: apenas se animacao nao é nil e está executando
    func stop(){
        if(self.animationView.animation != nil && self.animationView.isAnimationPlaying){
            self.animationView.stop()
        }
    }
    
}
