//
//  ConversaoTableViewCell.swift
//  IN8
//
//  Created by eWorld Tecnologia on 11/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

class ConversaoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMoeda: UILabel!
    @IBOutlet weak var lblValor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configuraCell(key : String, value : Double){
        self.lblMoeda.text = key
        
        self.lblValor.text = String(value)
    }
}
