//
//  HistoricoConversaoTableViewCell.swift
//  IN8
//
//  Created by eWorld Tecnologia on 13/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

class HistoricoConversaoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtMoedaDe: UILabel!
    @IBOutlet weak var txtMoedaPara: UILabel!
    @IBOutlet weak var txtValorDe: UILabel!
    @IBOutlet weak var txtValorPara: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configuraCell(conversao : ConversaoLocalModel){
        if let moedaDe = conversao.MoedaBase, conversao.MoedaBase != nil && conversao.MoedaBase != ""{
            self.txtMoedaDe.text = moedaDe
        }
        else{
            self.txtMoedaDe.text = "N/D"
        }
        
        if let moedaPara = conversao.MoedaTarget, conversao.MoedaTarget != nil && conversao.MoedaTarget != ""{
            self.txtMoedaPara.text = moedaPara
        }
        else{
            self.txtMoedaPara.text = "N/D"
        }
        
        if let valorDe = conversao.ValorBase, conversao.ValorBase != nil{
            self.txtValorDe.text = String(valorDe)
        }
        else{
            self.txtValorDe.text = "N/D"
        }
        
        if let valorPara = conversao.NovoValorGerado, conversao.NovoValorGerado != nil{
            self.txtValorPara.text = String(valorPara)
        }
        else{
            self.txtValorPara.text = "N/D"
        }
        
    }
    
}
