//
//  LoginViewController.swift
//  IN8
//
//  Created by eWorld Tecnologia on 12/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!{
        didSet{
            let tapDismiss = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            self.backgroundView.addGestureRecognizer(tapDismiss)
        }
    }
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtSenha: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func BtEntrar(_ sender: UIButton) {
        if(verificaCampos()){
            let emailExiste = Database.instance.verificaEmailExistente(email: txtEmail.text!)
            if(emailExiste != nil){
                if(!emailExiste!){
                    showAlertaController(self, texto: "Nenhum usuário encontrado!", titulo: "Atenção", dismiss: false)
                    return
                }
            }
            let pessoa = Database.instance.getPessoa(email: self.txtEmail.text!, senha: self.txtSenha.text!)
            print(pessoa)
            if(pessoa != nil && pessoa?.Nome != nil){
                UserDefaults.standard.set(pessoa?.PessoaId, forKey: "PessoaId")
                let alertController = UIAlertController(title: "Login realizado com sucesso", message: "Você será redirecionado!", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true) { }
            }
            else{
                showAlertaController(self, texto: "Algo errado aconteceu. Se o problema persistir por favor contate o suporte.", titulo: "Atenção", dismiss: false)
            }
        }
    }
    
    @IBAction func BtCadastrar(_ sender: UIButton) {
        if(verificaCampos()){
            let resultadoAddPessoa = Database.instance.addPessoa(nome: "", email: self.txtEmail.text!, senha: self.txtSenha.text!)
            if(resultadoAddPessoa != nil){
                if(resultadoAddPessoa == -100){
                    showAlertaController(self, texto: "Esse e-mail já está em uso.", titulo: "Atenção", dismiss: false)
                }
                else{
                    UserDefaults.standard.set(resultadoAddPessoa, forKey: "PessoaId")
                     let alertController = UIAlertController(title: "Cadastro realizado com sucesso", message: "Você será redirecionado!", preferredStyle: .alert)
                     let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                         self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                     }
                     alertController.addAction(OKAction)
                     self.present(alertController, animated: true) { }
                }
            }
            else{
                 showAlertaController(self, texto: "Algo errado aconteceu. Se o problema persistir por favor contate o suporte.", titulo: "Atenção", dismiss: false)
            }
        }
    }
    
    @IBAction func BtToggleSenha(_ sender: UIButton) {
        self.txtSenha.isSecureTextEntry.toggle()
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
       
    func verificaCampos() -> Bool{
        if(txtSenha.text != nil && txtSenha.text != "" && txtEmail.text != nil && txtEmail.text != ""){
            return true
        }
        else{
            showAlertaController(self, texto: "Por favor preencha todos os campos!", titulo: "Atenção", dismiss: false)
            return false
        }
    }
}
