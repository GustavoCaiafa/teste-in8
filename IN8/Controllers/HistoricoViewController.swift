//
//  HistoricoViewController.swift
//  IN8
//
//  Created by eWorld Tecnologia on 13/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit

class HistoricoViewController: UIViewController {
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var tableView: UITableView!
    var qtdeItens = 0
    var conversoesModel = [ConversaoLocalModel]()
    var pessoaid : Int64 = 0
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HistoricoConversaoTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoricoConversaoTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(Utils.Global.adicionouConversao || isFirstLoad){
            getConversoes()
        }
    }
    
    func getConversoes(){
        DispatchQueue.main.async {
            self.loadingView.play()
        }
        if(isFirstLoad){
            pessoaid = Int64(UserDefaults.standard.integer(forKey: "PessoaId"))
            isFirstLoad = false
        }
        let conversoesDB = Database.instance.getConversoes(pessoaid: pessoaid)
        if(conversoesDB != nil && conversoesDB?.first != nil){
            self.conversoesModel = conversoesDB!
            self.qtdeItens = self.conversoesModel.count
            self.tableView.reloadData()
            Utils.Global.adicionouConversao = false
        }
    }
    
    @IBAction func BtLogout(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Atenção", message: "Você será redirecionado!", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            UserDefaults.standard.removeObject(forKey: "PessoaId")
            let viewController = (self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")) as! LoginViewController
            viewController.modalPresentationStyle = .overFullScreen
            self.present(viewController, animated: true, completion: nil)
        }
        let CancelAction = UIAlertAction(title: "Cancelar", style: .destructive) { (action) in}
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true) { }
    }
    
}

extension HistoricoViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qtdeItens
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoricoConversaoTableViewCell", for: indexPath) as! HistoricoConversaoTableViewCell
        cell.configuraCell(conversao: conversoesModel[indexPath.row])
        return cell
    }
    
    
    
    
}
