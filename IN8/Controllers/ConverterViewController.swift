//
//  ConverterViewController.swift
//  IN8
//
//  Created by eWorld Tecnologia on 10/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import UIKit
import Lottie
import ObjectMapper
import Connectivity

class ConverterViewController: UIViewController,didSelectBaseProtocol {
    
    // Apos o delay da animacao para mover a pickerView para a direita, escondemos ela e setamos os textos aqui
    func selectedBase(didSelect: Bool, date: String?, isBase: Bool) {
        let delayTime = DispatchTime.now() + 0.4
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.basePickerView.isHidden = true
            self.basePickerView.centerXconstraint.constant = -500
        })
        if(didSelect){
            if(isBase){
                if(isFirstSearch){
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseIn, animations: {
                        self.selecionarValorViewConstraint.constant = 210
                        self.view.layoutIfNeeded()
                    }) { (animationComplete) in
                    }
                }
                self.txtMoedaBase.text = date
            }
            else{
                self.txtMoedaFinal.text = date
            }
        }
    }
    
    @IBOutlet weak var txtMoedaFinal: UITextField!{
        didSet{
            txtMoedaFinal.addTarget(self, action: #selector(abrirPickerViewMoedaFinal), for: .touchDown)
        }
    }
    
    @IBOutlet weak var txtMoedaBase: UITextField!{
        didSet{
            txtMoedaBase.addTarget(self, action: #selector(abrirPickerView), for: .touchDown)
        }
    }
    @IBOutlet weak var dismissKeyboardView: UIView!{
        didSet{
            let tapDismiss = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            self.dismissKeyboardView.addGestureRecognizer(tapDismiss)
        }
    }
    @IBOutlet weak var txtMoedaDesejada: UITextField!
    @IBOutlet weak var viewResultados: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var basePickerView: BasePickerView!
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var selecionarValorViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtValor: UITextField!
    fileprivate let connectivity: Connectivity = Connectivity()
    var conversaoModel = ConversaoModel()
    var qtdeItens = 0
    var isFirstLoad = true
    var pickerViewKeys = [String]()
    var pickerViewValues = [Double]()
    var isFirstSearch = true
    var pessoaModel = PessoaModel()
    
    // Ao carregar a view, fazemos uma chamada na API para pegar todas as moedas disponíveis e mostrar no popup
    override func viewDidLoad() {
        super.viewDidLoad()
        connectivity.connectivityURLs = [URL(string: "https://www.apple.com/library/test/success.html")!]
        verificaConexaoGetRates(base: "EUR")
        tableView.register(UINib(nibName: "ConversaoTableViewCell", bundle: nil), forCellReuseIdentifier: "ConversaoTableViewCell")
    }
    
    @objc func dismissKeyboard(){
        self.dismissKeyboardView.isHidden = true
        self.view.endEditing(true)
    }
    
    // Ao carregar a view, verificamos no cache se o usuário já é cadastrado, senao redirecionamos para cadastrar
    override func viewDidAppear(_ animated: Bool) {
        let isCadastrado = UserDefaults.standard.integer(forKey: "PessoaId")
        if(isCadastrado == 0 || isCadastrado == -100){
            let alertController = UIAlertController(title: "Notamos que você não é cadastrado!", message: "Para prosseguir por favor faça seu cadastro.", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                let viewController = (self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")) as! LoginViewController
                viewController.modalPresentationStyle = .overFullScreen
                self.present(viewController, animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true) { }
        }
    }
    
    // MARK: - Responsaveis pela verificao de internet e chamada na API
    func verificaConexaoGetRates(base : String){
        self.startLoading()
        connectivity.checkConnectivity { resultado in
            print("Resultado conexao: \(resultado.status)")
            self.getRates(status: resultado.status, base: base)
        }
    }
    
    // Chamada na api e configuracao da pickerview para mostrar ao usuario as moedas disponiveis
    func getRates(status : ConnectivityStatus, base : String){
        let parametros = ["":""] as [String : AnyObject]
        Service.callMethodJson(self, metodo: .get, parametros: parametros, url: Service.LinksAPI.GetRates + base, nomeCache: "rates", status: status) { (response, error) in
            print(response)
            self.stopLoading()
            if(error == nil){
                if let cvsModel = Mapper<ConversaoModel>().map(JSONObject: response){
                    self.conversaoModel = cvsModel
                    self.qtdeItens = self.conversaoModel.Rates?.count ?? 0
                    if(self.isFirstLoad){
                        self.configuraData()
                    }
                    else{
                        let valor = Double(self.txtValor.text!)
                        if(valor! > 0){
                            self.pickerViewValues = [Double]()
                            for value in self.conversaoModel.Rates!.values{
                                self.pickerViewValues.append(value * valor!)
                            }
                            self.pickerViewKeys = [String]()
                            for key in self.conversaoModel.Rates!.keys {
                                self.pickerViewKeys.append(key)
                            }
                            self.addConversaoDB(valorbase: valor!)
                            self.tableView.reloadData()
                        }
                        else{
                            showAlertaController(self, texto: "O número deve ser maior que 0!", titulo: "Atenção", dismiss: false)
                        }
                    }
                }
                else{
                    showAlertaController(self, texto: "Algo errado aconteceu.Por favor tente novamente!", titulo: "Atenção", dismiss: false)
                }
            }
            else{
                print(error?.localizedDescription)
                showAlertaController(self, texto: "Algo errado aconteceu.Por favor tente novamente!", titulo: "Atenção", dismiss: false)
            }
        }
    }
    
    func addConversaoDB(valorbase : Double){
        var valorFinal : Double = 0
        let pessoaid = Int64(UserDefaults.standard.integer(forKey: "PessoaId"))
        let filtro = self.conversaoModel.Rates!.first { $0.key == self.txtMoedaFinal.text }
        if(filtro != nil){
            valorFinal = filtro!.value * valorbase
        }
        self.txtMoedaDesejada.text = String(valorFinal)
        if(Database.instance.addConversao(moedabase: self.txtMoedaBase.text!, valorbase: valorbase, moedatarget: self.txtMoedaFinal.text, novovalorgerado: valorFinal, estrangeirapessoaid: pessoaid)){
            Utils.Global.adicionouConversao = true
        }
        else{
            showAlertaController(self, texto: "Não conseguimos salvar sua conversão. Por favor tente novamente!", titulo: "Algo errado aconteceu.", dismiss: false)
        }
        self.viewResultados.isHidden = false
    }
    
    // passamos os valores para a pickerView e setamos delegate
    func configuraData(){
        pickerViewKeys = [String]()
        for key in self.conversaoModel.Rates!.keys {
            pickerViewKeys.append(key)
        }
        pickerViewKeys.append("EUR") // adicionamos por ser a base utilizada na chamada
        pickerViewKeys.sort()
        self.basePickerView.configuraPickerView(data: pickerViewKeys, delegate: self)
        self.isFirstLoad = false
    }
    
    @IBAction func BtConverter(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txtValor.text != nil && txtValor.text != "" && txtMoedaBase.text != nil && txtMoedaFinal.text != ""){
            if(txtMoedaBase.text == txtMoedaFinal.text){
                showAlertaController(self, texto: "As moedas não podem ser iguais.", titulo: "Atenção", dismiss: false)
                return
            }
            verificaConexaoGetRates(base: txtMoedaBase.text!)
        }
        else{
            showAlertaController(self, texto: "Por favor preencha todos os campos!", titulo: "Atenção", dismiss: false)
        }
    }
    
    func startLoading(){
        self.loadingView.isHidden = false
        self.loadingView.play()
    }
    
    func stopLoading(){
        self.loadingView.isHidden = true
        self.loadingView.stop()
    }
    
    // Recolhemos o teclado e mostramos a pickerView informando que é a escolha da moeda base
    @objc func abrirPickerView(){
        self.view.endEditing(true)
        DispatchQueue.main.async {
            self.basePickerView.isMoedaBase = true
            self.basePickerView.isHidden = false
            self.basePickerView.mostrarPickerView()
        }
    }
    
    @objc func abrirPickerViewMoedaFinal(){
        self.view.endEditing(true)
        DispatchQueue.main.async {
            self.basePickerView.isMoedaBase = false
            self.basePickerView.isHidden = false
            self.basePickerView.mostrarPickerView()
        }
    }
    
}

extension ConverterViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qtdeItens
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversaoTableViewCell", for: indexPath) as! ConversaoTableViewCell
        cell.configuraCell(key: pickerViewKeys[indexPath.row], value: pickerViewValues[indexPath.row])
        return cell
    }
    
}

extension ConverterViewController : UITextFieldDelegate{
    
    // Verificamos o textieldValor pois nao queremos que a pessoa digite nele, apenas abra o popup
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == txtValor){
            self.dismissKeyboardView.isHidden = false
            return true
        }
        return false
    }
}
