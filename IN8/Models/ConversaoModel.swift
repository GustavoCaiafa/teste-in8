//
//  ConversaoModel.swift
//  IN8
//
//  Created by eWorld Tecnologia on 10/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import Foundation
import ObjectMapper

public class ConversaoModel: Mappable{
    public var Base : String? = "N/D"
    public var Date : String? = "N/D"
    public var Rates : Dictionary<String,Double>?
    
    required public init?(map: Map) {
    }
    
    init() {
    }
    
    public func mapping(map: Map) {
        Base <- map["base"]
        Date <- map["date"]
        Rates <- map["rates"]
    }
    
}


