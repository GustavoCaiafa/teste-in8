//
//  ConversaoLocalModel.swift
//  IN8
//
//  Created by eWorld Tecnologia on 13/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import Foundation

class ConversaoLocalModel {
    public var ConversaoId : Int64?
    public var MoedaBase : String?
    public var ValorBase : Double?
    public var MoedaTarget : String?
    public var NovoValorGerado : Double?
    
    init(conversaoid : Int64,moedabase : String?,valorbase : Double?, moedatarget: String?, novovalor: Double?) {
        self.ConversaoId = conversaoid
        self.MoedaBase = moedabase
        self.ValorBase = valorbase
        self.MoedaTarget = moedatarget
        self.NovoValorGerado = novovalor
    }
    
    init(){
    }
    
}
