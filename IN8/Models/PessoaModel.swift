//
//  PessoaModel.swift
//  IN8
//
//  Created by eWorld Tecnologia on 12/03/20.
//  Copyright © 2020 Gustavo. All rights reserved.
//

import Foundation
import UIKit

class PessoaModel {
    public var PessoaId : Int64?
    public var Nome : String?
    public var Email : String?
    public var Senha : String?
    
    init(pessoaid : Int64,nome : String?,email : String?, senha: String?) {
        self.PessoaId = pessoaid
        self.Nome = nome
        self.Email = email
    }
    
    init(){
        
    }
    
}
